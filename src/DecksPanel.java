/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;

public class DecksPanel extends JPanel {
    public final double MASTER_THRASHER = 60;
    public final double DICTATOR = 45;
    public final double STREET_KING = 50;

    private JRadioButton masterThrasher;
    private JRadioButton dictator;
    private JRadioButton streetKing;
    private ButtonGroup decksGroup;

    public DecksPanel() {
        setLayout(new GridLayout(3, 1));

        masterThrasher = new JRadioButton("The Master Thrasher", true);
        dictator = new JRadioButton("The Dictator");
        streetKing = new JRadioButton("The Street King");

        decksGroup = new ButtonGroup();
        decksGroup.add(masterThrasher);
        decksGroup.add(dictator);
        decksGroup.add(streetKing);

        setBorder(BorderFactory.createTitledBorder("Decks"));

        add(masterThrasher);
        add(dictator);
        add(streetKing);
    }

    public double getDeckCost() {
        double deckCost;

        if (masterThrasher.isSelected())
            deckCost = MASTER_THRASHER;
        else if (dictator.isSelected())
            deckCost = DICTATOR;
        else
            deckCost = STREET_KING;
   
      return deckCost;
   }
}